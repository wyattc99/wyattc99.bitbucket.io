# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 15:30:01 2021
@package lab 0x05
@breif      This is a file for I2C driver
@detail     This file interacts with a BNO055 Sensor in order to calibrate the
            sensor and from there either change the operating mode, read the
            euler angle data, or read the angular velocity data.
@author     James Lavelle Wyatt Conner
@file       lavelle.py
@brief      This file will interact with a BNO055 Sensor
@details    This file interacts with a BNO055 Sensor in order to calibrate the
            sensor and from there either change the operating mode, read the
            euler angle data, or read the angular velocity data.
@author     James Lavelle Wyatt Conner
@date       11 November 2021
"""

import struct

class driver:
    
    def __init__(self, address, i2c):
        """
        @brief      This function will create an i2c object
        @details    This function will establish an i2c object that will work
                    throughout the class.
        @param i2c  The i2c object that will be associated with this class.
        """
        
        self.address = address
        self.i2c = i2c

    def set_operating_mode(self, mode):
        """
        @brief      This function will set the mode
        @details    This function will set the mode to NDOF at the number 12.
        @param mode The mode that the driver will need to be set at.
        """
        
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_cal_status(self):
        """
        @brief      This will read data of calibration status of I2C unit
        @details    Depending upon the data read from the calibration status
                    we can determine if the system has been properly calibrated.
        """
        
        buf_cal = bytearray(1)      
        cal_bytes = self.i2c.mem_read(buf_cal, 0x28, 0x35) 
        cal_status = (cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)
        
        return cal_status   
        
    def get_cal_coef(self):
        """
        @brief      This will return our calibration coefficents to allow us to properly read data
        @details    This allows for us read the calibration offsets for each sensor and will return
                    the data in an array format. 
        @return     This will return an array of the calibration coefficents. 
        """
        
        buf = bytearray(22)
        self.i2c.mem_read(buf, 0x28, 0x55)
        return tuple(struct.unpack('<hhhhhhhhhhh',buf))
        
    def write_cal_coef(self, cal_coef):
        """
        @brief      This will write to the calibration offsets
        @details    This will allow us to write to the calibration coefficents
                    to set them to what we want, as we can save the last calibration
                    status and use them again once we reset the system.
        @param      cal_coef  this is 18 byte input that will set the values 
                    of the calibration coefficents
                    
        """
        
        self.i2c.mem_write(cal_coef, 0x28, 0x55)
        pass
        
    def read_euler(self):
        """
        @brief      This will read the euler angle based off the I2C unit
        @details    This will read the euler angle data from the I2C unit.
                    It will read the data for all 3 axis of rotation, pitch
                    yawn and roll
        """
        
        self.buf_euler = bytearray(6)
        self.i2c.mem_read(self.buf_euler, 0x28, 0x1a)
        self.euler_ang = struct.unpack('hhh', self.buf_euler)
        return self.euler_ang

    def read_angular(self):
        """
        @brief      This will read angualr veolicty data from the I2C unit
        @details    This will read the angular velocity data from the I2C unit
                    for all 3 axis of rotation, the output is a 3 number array.
                    
        """
        
        self.buf_ang = bytearray(6)
        self.i2c.mem_read(self.buf_ang, 0x28, 0x14)
        self.angular_vel = struct.unpack('hhh', self.buf_ang)
        return self.angular_vel