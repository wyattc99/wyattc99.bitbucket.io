# -*- coding: utf-8 -*-
"""
@package    Term_Project
@author     Wyatt Conner
@author     James Lavelle
@date       9 December 2021
@brief      Term project report and description
@details    The ball balancing program consists of two parts. The first part is
            being able to read from resistive touch panels. The second part is 
            to perform the balancing of a ball on a touchpad. These two goals 
            were met by first developing a driver for the touchpad. Our task 
            diagram for the project is below

            \image html Term_Task_Diagram.jpg width=25%
            Term Project Report

            This driver
            is able to turn on four pins, and then locate a point of contact and
            determine this location on the touchpad. The driver then returns a 
            tuple of the 3 coordinates. The next file used is a driver that 
            creates motor objects for the two motors used for this project. Then
            an I2C driver was used to calibrate and collect data from the IMU. 
            These two files were copied from labs 3 and 4. Then several task 
            files were implemented in order to perform several different tasks.
            The first task file is the calibration task. This task is able to 
            calibrate both the touchpad and the IMU and then return a new file 
            with the calibrated data. The next task file is the data collection
            task. This file is able to multiply a correct coefficient through 
            the data collected from the calibration task and then output this 
            file into a usable form of data for the speed controller. This speed
            controller task uses all of the other tasks in order to both balance
            the platform and the ball. This controller takes the calibrated data
            and correct coefficients and then calculates the necessary duty and
            gain needed to balance the ball on the platform. The state transition
            diagram for the speed controller is below
            
            \image html Speed_control_state.jpg width=25%
            
            The last task is the user task. This task has several states and a 
            user inputted key will transition from one state to the next, whether 
            it is to balance the ball, turn off the motor, collect data, or 
            balance the platform. To run all of these tasks, a main file was 
            created that first establishes all of the class objects and then 
            uses a shares.py file to run separate tasks. This main file is able
            to balance the ball with some inaccuracy as depicted by the video. 
            Additional files include a read and write file that can convert 
            strings to integers or hex or vice versa. It can also read and write
            text files and then output a new file based on the need of the program.
            the state diagram for the user task is below.

            \image html Term_User_state.jpg width=25%
            
            We tested for the correct calibration values although we did not 
            include these because they were not required and we did not find 
            perfect values for the system. Given more time, correct calibration 
            and coefficient values could be found to better balance the ball on 
            the platform. A video demonstration is avliable below.  
            
            \htmlonly
            <iframe width="560" height="315" 
            src="https://www.youtube.com/embed/Z_2EdOBBIzs"
            title="YouTube video player" frameborder="0"
            allowfullscreen></iframe>
            \endhtmlonly
            
@file       main.py
@brief      This main file will run the ball balancing program
@details    This main file will run the ball balancing program by first 
            importing python tools and then by importing all of the other 
            files used throughout the program. Then, it will create objects 
            and use these objects to execute a total of five tasks. These 
            tasks will effectively balance a ball using motors, an IMU, and a 
            touch pad.
@author     Wyatt Conner
@author     James Lavelle
@date       8 December 2021
"""

# Import General files
import shares
import pyb
from pyb import I2C
import utime

# Import Classes of each driver and Task
from Driver_I2C import I2C_Driver
from Driver_Touch import Touch_Driver
from Driver_Motor import Driver
from Driver_Motor import Motor
from Task_Calibration import calibration
from Task_Data_Collection import Data_Collection
from Task_Speed_Controller import Task_Controller
from Task_User import user_task

# Create objects of each driver
## Creating the I2C object
i2c = I2C(1, I2C.MASTER)
i2c.init(I2C.MASTER, baudrate = 500000)
## creating the IMU object
IMU = I2C_Driver(i2c, 0x28)

## Creating the touch panel object
touch = Touch_Driver()

## Creating Shared Motor Driver
Motor_Drive = Driver()

## Creating the Motor Driver for Motor 1
Motor1 = Motor(pyb.Pin (pyb.Pin.cpu.B4), pyb.Pin (pyb.Pin.cpu.B5), 1, 2, 3)

## Creating the Motor Driver for Motor 2
Motor2 = Motor(pyb.Pin (pyb.Pin.cpu.B0), pyb.Pin (pyb.Pin.cpu.B1), 3, 4, 3)

## State of the Code send different values to set different states
Flag = shares.Share(0)
# 0 - restart
# 1 - ball on
# 2 - ball off
# 3 - Level platform

## Share object for the x position of the ball
xcord = shares.Share(0)

## Share object for the y position of the ball
ycord = shares.Share(0)

## share object of the z cord of the touch pad
zcord = shares.Share(0)

## Share object for the angle of pitch
pitch = shares.Share(0)

## Share object for the angle of roll
roll = shares.Share(0)

## Share object for angular velocity of pitch
pitch_vel = shares.Share(0)

## Share object for angular velocity of roll
roll_vel = shares.Share(0)

## Calibration Task object
Task_1 = calibration(Flag, IMU, touch)
## Data collection task object
Task_2 = Data_Collection(Flag, xcord, ycord, zcord, pitch, roll, pitch_vel, roll_vel, IMU, touch, Task_1)
## Task controller task object for motor 1
Task_3 = Task_Controller(Flag, 1, xcord, ycord, zcord, pitch, roll, roll_vel, pitch_vel, Motor1, Motor_Drive)
## Task controller task object for motor 2
Task_4 = Task_Controller(Flag, 2, xcord, ycord, zcord, pitch, roll, roll_vel, pitch_vel, Motor2, Motor_Drive)
## Task user object for the user task
Task_5 = user_task(Flag, xcord, ycord, zcord, pitch, roll, roll_vel, pitch_vel, Motor_Drive)

if __name__ == '__main__':  
    while True:
        Task_1.run()
        Task_2.run()
        Task_3.run()
        Task_4.run()
        Task_5.run()
      