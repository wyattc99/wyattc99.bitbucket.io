# -*- coding: utf-8 -*-
"""
@package    Term_Project
@file       Task_Calibration.py
@brief      This task will calibrate the IMU and the touch pad.
@details    This task file will calibrate the IMU and the touch pad by 
            creating a class object called calibration. This class will run a
            file through the IMU and touch pad and output a new file with the 
            correctly calibrated coefficients needed to balance the ball.
@author     Wyatt Conner
@author     James Lavelle
@date       8 December 2021
"""

# imports of the internal python programs as well as other files used 
import os
from pyb import I2C
import utime
from read_and_write import Read_Write_Text
import struct
from shares import Share

class calibration:
    """
    @brief      This class will generate a calibration task        
    @details    This class will generate a calibration task that can be created
                by the main program. This task can then be called to run and it
                calibrate both the touch panel and IMU to get correction factors
                for each. 
    """
    def __init__(self, Flag, IMU, touch):
        """
        @brief          This the function that initializes the task object
        @details        This function when called will create a new calibration
                        task object.
        @param  Flag    This is the flag of the system, allow task to 
                        communicate what task they are currently in so they 
                        can work properly
        @param  IMU     This is passing in the IMU driver object from the main 
                        file
        @param  touch   This is passing in the touch panel driver object from 
                        the main file
        """
        
        # Define inputs
        ## the flag used throughout the system
        self.FLag = Flag
        ## the imu of the i2c
        self.IMU = IMU
        ## the touch pad
        self.touch = touch
        
        # Define internal variables
        ## variable for the file name that stores the touch panel coefficents
        self.filename_RT = "RT_cal_coeffs.txt"
        ## variable for the file name that stores the IMU coefficents
        self.filename_IMU = "IMU_cal_coeffs.txt"
        ## This is a flag variable used to check if the IMU is calibrated
        self.IMU_status = 0
        ## This is a flag that show if the touch panel is calibrated or not
        self.RT_status = 0
        ## This is a flag that show if the IMU is calibrated or not
        self.runs = 0
        # Task to update the RT calibration status
        
    def run(self):
        """
        @brief      This function will run the task of data calibration.
        @details    This function will run the data calibration task by having
                    a series of different runs that will take data from a file
                    and then run it through the IMU of touch pad to create a 
                    new file. This new file will have the calibrated 
                    coefficients that are necessary for the ball balancing 
                    system. 
        """
        
        self.runs += 1
        if self.runs == 1:
            
              # Code to update the RT calibration
            if self.filename_RT in os.listdir():
                  # read the data from the calibration file
                  self.RT = Read_Write_Text().read_int(self.filename_RT)
                  # updata rt coef shared variable
                  #self.RT = [int(self.RT) for self.RT in self.RT]
                  self.RT_status = 1
                  print(self.RT)
                  utime.sleep(1)
            else: 
                  # Run the calibration
                  print('press the touch panel in the center')
                  utime.sleep(3)
                  while self.center_x == 0 or self.center_y ==0:
                      self.center_x = self.xcord
                      self.center_y = self.ycord
                  
                  print('press opposite corner of orgin')
                  utime.sleep(3)
                  while self.point_x1 == self.center_x or self.point_y1 == self.center_y:
                      self.point_x1 = self.xcord
                      self.point_y1 = self.ycord
                
                  print('press the corner of the orgin')
                  utime.sleep(3)
                  while self.point_x2 == 0 or self.point_y2 == 0:
                      self.point_x1 = self.xcord
                      self.point_y1 = self.ycord
                
                
                      
                  
                  # Write data to the calibration file
                  Read_Write_Text().write_int(self.filename_RT, self.RT)
                
                
                
            # Code to update the IMU calibration
            if self.filename_IMU in os.listdir():
                # Read data from calibration file
                self.IMU_coef = Read_Write_Text().read_int(self.filename_IMU)
                print(self.IMU_coef)
                # Write the calibration data to the IMU
                #self.IMU_coef = struct.pack('hhhhhhhhhhhhhhhhhh'.format(len(self.IMU_coef)), *self.IMU_coef)
                #buf = struct.pack('%sf' % len(floatlist), *floatlist)
                
                self.IMU_c = struct.pack('%sf' % len(self.IMU_coef), *self.IMU_coef)
                self.IMU.write_cal_coef(self.IMU_c)
                self.IMU_status = 1
                self.mode = 12
                self.IMU.set_operating_mode(self.mode)
            else:
                # Run the calibration
                self.cal = False
                self.mode = 12
                self.IMU.set_operating_mode(self.mode)
                
                while( self.cal == False):
                    try:
                        self.status = self.IMU.get_cal_status()
                        print('Starting IMU calibration\n')
                        print('Move the IMU until both numbers are a 3\n')
                        print ('Gyro Calibration', self.status[0], 'Accle Calibration', self.status[1] )
                        utime.sleep(1)
                        
                        if (self.status[0] == 3 and self.status[1] == 3):
                            self.mode = 0
                            self.IMU.set_operating_mode(self.mode)
                            
                            self.IMU_coef = self.IMU.get_cal_coef()
                            print(self.IMU_coef)
                            utime.sleep(1)
                            self.mode = 12
                            self.IMU.set_operating_mode(self.mode)
                            self.cal = True
                    except KeyboardInterrupt:
                        break
                        
                # Write data to calibration file
                Read_Write_Text().write_hex(self.filename_IMU ,self.IMU_coef)
                
            
            self.status = self.IMU_status + self.RT_status
            
            if self.status != 2:
                print('Calibration Complete, However power board restart is required. \n')
                #self.Flag.write(0)
            else:
                print('Calibration Complete')

        else: 
            pass
        
    def get_k_RT(self):
        """
        @brief      This the function call the RT coefficents in an array
        @details    This function will return the RT coefficents in an array 
                    to simplify sharing the variable, as the shares object 
                    can't be an array.
        @return     This will return the RT coefficient data.
        """
        
        ## the RT coefficients
        return(self.RT)
        
   
                