# -*- coding: utf-8 -*-
"""
@package    Term_Project
@file       Driver_Motor.py
@brief      This file will develop a motor driver.
@details    This file will use two class objects to develop a driver for two 
            separate motors. These class objects will have functions built in 
            that can enable or disable the motors. Additionally, these objects
            can clear any faults that may come up.
@author     James Lavelle 
@author     Wyatt Conner
@date       8 December 2021
"""

# Import of utime and pyb
import utime
import pyb

class Driver:
    """
    @brief      A motor driver class for the DRV8847 from TI.
    @details    Objects of this class can be used to configure the DRV8847
                motor driver and to create one or moreobjects of the
                Motor class which can be used to perform motor
                control.
    """
    
    def __init__ (self):
        """
        @brief      Initializes and returns a DRV8847 object.
        @details    This function will contruct a DRV8847 object with 
                    initializing the parameters. Objects of this class can 
                    be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor
                    control.
        """
        
        ## the sleep function of the motor
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        ## the fault function of the motor
        self.nFAULT = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN)
        # self.faultInt = pyb.ExtInt(self.nFAULT, mode = pyb.ExtInt.IRQ_FALLING,
        #                          pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        ## the button of the motor
        self.button = pyb.Pin.cpu.C13
        #self.buttonInt = pyb.ExtInt(self.button, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE,
        #                           callback = self.buttonPressed)
        ## Pin A 15 used to enabled an disable motors
        #self.pinA15 = pyb.Pin (pyb.Pin.cpu.A15)
        ## Sets up the timer 3 for the motors
        self.tim3 = pyb.Timer(3, freq = 20000)
        ##pinC13 is a variable to define the pin being used for the button.
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    def enable (self):
        """
        @brief      Brings the DRV8847 out of sleep mode.
        @details    This function will enable the DRV8847 to be on and out
                    out of its sleep mode. Also has a 25 micro second 
                    delay time in order to avoid faults.
        """
        
        ## Pin A 15 used to enabled an disable motors
        # self.faultInt.disable()
        self.nSLEEP.high()
        utime.sleep_us(25)
        # self.faultInt.enable()
        # self.pinA15 = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        #self.pinA15.high()
        print('Motors Enabled')
        
    def disable (self):
        """
        @brief      Puts the DRV8847 in sleep mode.
        @details    This function will turn the DRV8847 motor off by 
                    turning it into sleep mode.
        
        """
        
        ## Pin A 15 used to enabled an disable motors
        self.nSLEEP.low()
        #self.pinA15 = pyb.Pin (pyb.Pin.cpu.A15)
        #self.pinA15.low()
        print('Motors Disabled')
        utime.sleep_us(25)
        
    # def fault_cb (self, IRQ_src):
    #     ''' @brief      Callback function to run on fault condition.
    #         @details    This function will enable a callback funtion to 
    #                     run a fault condition in case the motor is faulting.
    #         @param      IRQ_src The source of the interrupt request.
    #     '''
    
        
    #     #if IRQ_src == True:
    #         ## Pin A 15 used to enabled an disable motors
    #         #self.pinA15 = pyb.Pin (pyb.Pin.cpu.A15)
    #         #self.pinA15.low()
    #     print('fault has occcured')
    #     self.nSLEEP.low()
        
    #     #elif IRQ_src == False:
    #         #pass
    
   # def motor (self):
        # """
        # @brief              Initializes and returns a motor object associated
        #                     with the DRV8847.
        # @details            This function will initialize the object 
        #                     associated with the DRV8847 in order to have the 
        #                     motor function.
        #     @param motor    The motor being initialized and returned.
        #     @return         An object of class Motor
        # """
        
       
class Motor:
    """
    @brief      A motor class for one channel of the DRV8847.
    @details    Objects of this class can be used to apply PWM to a given
                DC motor.
    """
    
    def __init__ (self, pin1, pin2, ch1, ch2, Timer):
        """
        @brief          Initializes and returns a motor object associated 
                        with the DRV8847.
        @details        Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        @param pin1     The first pin on the board.
        @param pin2     The second pin on the board.
        @param ch1      The first channel of the motor.
        @param ch2      The second channel of the motor.
        @param Timer    The timer asscociated with the proper channels for 
                        the motor
        """
        
        ## the first pin
        self.pin1 = pin1
        ## the second pin
        self.pin2 = pin2
        ## the first channel
        self.ch1 = ch1
        #3 the second channel
        self.ch2 = ch2
        ## the timer used
        self.Timer = Timer
      
    def set_duty (self, duty):
        """
        @brief          Set the PWM duty cycle for the motor channel.
        @details        This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
        @param  duty    A signed number holding the duty cycle of the PWM 
                        signal sent to the motor
        """
        
        ## duty cycle for the motor selected
        self.duty = duty
        
        if abs(self.duty) > 100:
            self.duty1 = 0
            self.duty2 = 0
            print('Please enter a valid input')
            
        if self.duty <= 0:
            self.duty2 = 100
            self.duty1 = 100 + self.duty
            #print('negative')
            #print(self.duty)

        elif self.duty > 0:
            self.duty1 = 100
            self.duty2 = 100-self.duty
            #print('positive')
            print(self.duty)
            
        self.tim3 = pyb.Timer(self.Timer, freq = 20000)  
        ## Defining Timer 3 channel 1 of motor selected
        t3ch1 = self.tim3.channel(self.ch1, pyb.Timer.PWM, pin=self.pin1)
        ## Setting duty cycle of correct pin and channel of the motor
        t3ch1.pulse_width_percent(self.duty1)
        ## Setting duty cycle of correct pin annd channel of the motor
        t3ch2 = self.tim3.channel(self.ch2, pyb.Timer.PWM, pin=self.pin2)
        t3ch2.pulse_width_percent(self.duty2)
        
        pass
    
if __name__ == '__main__':
    
    # Adjust the following code to write a test program for your motor class.
    # Any code within the if __name__ == '__main__' block will run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing the pins and timer objects needed
    # Enable the motor driver    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle
    # of the second motor to 60 percent
    
    ## motor class object
    motor_driver = Driver()
    ## the first motor
    motor_1 = Driver.Motor()
    ## the second motor
    motor_2 = Driver.Motor()
    
    # enabling the motor
    motor_driver.enable()

    motor_1.set_duty(40)
    motor_2.set_duty(60)
   