''' 
@package    Lab_0x02


@file       task_encoder.py
@brief      Encoder task to output data from encoder driver.
@details    Implements a finite state machine for the encoder task to 
            communicate with the encoder driver to update positions, delta
            and reset the position to zero.
@author     Wyatt Conner 
@author     James Lavelle
@date       October 20, 2021
 
'''

import utime, pyb
from micropython import const
from encoder import driver_encoder


class encoder_task():
    ''' 
        @brief      Encoder task that calls the encoder driver.
        @details    Implements a finite state machine that determines the position
                    delta and set the position of the encoder. 
    '''
    
    def __init__(self, flag, period, POS_share, delta_share):
        ''' 
            @brief              Constructs an encoder task.
            @details            The encoder task is implemented as a finite state
                                machine.
            @param flag         The flag of the task to zero encoder
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param POS_share    A shares.Share object used to hold the Position.
            @param delta_share  A shares.Share object used to hole the delta. 
        '''
        
        ## The flag of the task
        self.flag = flag
        ## Ther period (in us) of the task
        self.period = period
        ## A shares.Share object representing Encoder Position
        self.POS_share = POS_share
        ## A shares.Share object representing Encoder Delta
        self.delta_share = delta_share
        ## A flag indicating if debugging print messages display
        
        
        ## A timer used for LED dimming.
        #
        #  This should really be in a seperate driver file, but for simplicity
        #  it is included here.
        self.Timer = 3
        ## PinParameter that calls the first pin of the encoder that matches with channel 1 of the pin.
        self.pin1_enc = pyb.Pin(pyb.Pin.cpu.C6)
        ## Parameter that calls the first pin of the encoder that matches with channel 2 of the pin.
        self.pin2_enc = pyb.Pin(pyb.Pin.cpu.C7)
        
        driver_encoder.__init__(self, self.pin1_enc, self.pin2_enc, self.Timer)
            
         ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        ''' 
        @brief     Runs one iteration of the FSM
        @details   This function will run one the encoder by using the driver encoder class.
        '''
        if self.flag.read() == True :
            driver_encoder.set_position(0,0,0) 
            self.flag.write(False)
            print('task_encoder enabled')
               
        driver_encoder.update(self)
        self.POS_share.write(driver_encoder.get_position(self))
        self.delta_share.write(driver_encoder.get_delta(self))
           
          
               
               
            