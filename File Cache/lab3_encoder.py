# -*- coding: utf-8 -*-
''' 
@package    Lab_0x03

@file encoder.py
@brief      A driver for reading from Quadrature Encoders
@details    A driver for the encoder to communicate with the hardware of the 
            encoder. It is capable of updating the position of the encoder and
            preventing overflow. It can also output the position and delta of
            the encoder. It can also set the position of the encoder. 
@author     Wyatt Conner
@author     James Lavelle
@date       Novemeber 18, 2021


'''
import pyb
import utime
import math

class driver_encoder():
      '''   
      @brief    Interface with quadrature encoders
      @details  Class that involves the driver for the encoder. It includes
                functions to get the position, and delta of the encoder.
      '''
      def __init__(self, pin1, pin2, ch1, ch2, Timer):
          '''
          @brief    Defining an encoder driver
          @details  A driver for encoders that communcates with the hardware.
          
          @param pin1       Parameter that calls the first pin of the encoder
                            that matches with channel 1 of the pin.
          @param pin2       Parameter that calls the first pin of the encoder
                            that matches with channel 2 of the pin.
          @param ch1        Parameter to determine the channel 1 of encoder
          @param ch2        Parameter to determine the channel 2 of encoder
          @param Timer      This will callout the corresponding timer that is
                            associated with the pins called out for the encoder.
          '''
          # Defining internal variables
          #
          ## Redefining in the pin 1 as self object
          self.pin1 = pin1
          ## Defining position intially as zero
          self.position = 0
          ## Defining variable count to start at zero
          self.count = 0
          ## Defining variable delta to start at zero
          self.delta = 0
          ## Defin the number of times update is used
          self.runs = 0
          ## Number of ticks the encoder does per full rotation this is used convert later
          self.Ticks_per_rot = 2000
         
         
          
          
          ## Time object for the encoder
          self.tim = pyb.Timer(Timer, prescaler=0, period=65535)
          ## Time object for channel 1 of the encoder
          self.tim_ch1 = self.tim.channel(ch1, pyb.Timer.ENC_AB, pin=pin1)
          ## Time object for channel 2 of the encoder
          self.tim_ch2 = self.tim.channel(ch2, pyb.Timer.ENC_AB, pin=pin2)
          
          
          
      def update(self,sys):
         ''' 
          @brief        Updates encoder position and delta
          @details      This updates the counter every time it is ran. This counter
                        has a 16 bit maximum count, therefore we also correct for
                        overflow by correcting the value which will be used to create
                        the true position of the encoder. 
         '''
         # define the initial parameters of the encoder.
         #
         self.runs += 1
         ## The count of previous iteration
         self.previous_count = self.count
         ## Updating the count for this iteration
         self.count= self.tim.counter()
         ## Delta variabel is previous count - count
         self.delta = self.count - self.previous_count
         
         if self.runs == 1:
             ## determines the start time
             self.start_time = utime.ticks_us()
         
         
         # update encoder position value for when overflow occur
         # If delta goes from 0 to 65535 we need to minus 65535
         if self.delta >= 65535/2:
                            self.delta -= 65535
                            
        
         
     # If delta goes from 65535 to 0 we need to add 65535
         if self.delta <= -65535/2:
                            self.delta += 65535
                            
         
         ## current position of the encoder   
         self.position += self.delta 
         
         # Convert position to radians
         #
         ## The position value in radians
         self.position_rad = round(self.position*math.pi*2/self.Ticks_per_rot,1)
         
         
       
         

      print('Reading encoder count and updating position and delta values')

      def get_position(self):
            ''' 
        @brief     Returns encoder position
        @details   This simply will output the current encoder position that is created
                   from the update function. 
        @return    The position of the encoder shaft
            '''
     
            return self.position_rad

      def set_position(self, POS, Speed): 
            ''' 
            @brief Sets        encoder position
            @details           This is used to set a datum of set the current position of the
                               encoder to some numeric value the user wishes to be. 
            @param position    The new position of the encoder shaft
            @param delta       The speed of the encoder by determining the change in 
                               position every period.
            '''
            self.position = POS
            self.delta = Speed
    
            print('Driver Set Position enabled')

      def get_delta(self):
            ''' 
            @brief     Returns encoder delta
            @details   This function simply outputs the current delta value that is
                       created in the update function. 
            @return    The change in position of the encoder shaft
                       between    the two most recent updates
                   '''
            
            return self.delta
