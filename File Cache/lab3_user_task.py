''' 
@package    Lab_0x02


@file       task_user.py
@brief      User interface task for cooperative multitasking example.
@details    Implements a finite state machine for the user task to ask the user
            what information they want from the encoder. 
@author     Wyatt Conner 
@author     James lavelle
@date       October 20, 2021
'''

import utime, pyb
from micropython import const
#from task_encoder import encoder_task

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)
## State 2 Gather Data for 30 seconds
S2_GATHER_DATA      = const(2)
## State 3 Print Data to putty
S3_PRINT_DATA       = const(3)


class task_user(): 
     ''' 
         @brief      User interface task for cooperative multitasking example.
         @details    Implements a finite state machine
     '''
    
     def __init__(self, sys_state, period, system, duty_share, POS1_share, 
                 delta1_share, POS2_share, delta2_share):
        ''' 
            @brief              Constructs an motor task.
            @details            The encoder task is implemented as a finite state
                                machine.
            @param sys_state    The state of the system
            @param period       determines the period of time this function will take
            @param system       determines what hardware set will be used
            @param duty_share   A shares.Share object used to hole the duty percent
            @param POS1_share   A shares.Share object used to hold the Position
                                of encoder 1.
            @param POS2_share   A shares.Share object used to hold the Position
                                of encoder 2.
            @param delta1_share     A shares.Share object used represent delta of
                                    encoder 1. 
            @param delta2_share     A shares.Share object used represent delta of
                                    encoder 2. 
        '''
        # Setting up the share variables between tasks
        #
        ## System state to determine when enable certain function act
        self.sys_state = sys_state
        # state 0 - null no commans
        # state 1 - zero encoder 1
        # state 2 - zero encoder 2
        # state 3 - fault motors
        # state 4 - enable motors
        # state 5 - disable motors
        # state 6 - set duty cycle
        
        ## Period of MCU to run on
        self.period = period
        ## Duty to determine duty cycle percent provided to the motor
        self.duty_share = duty_share
        ## Position of the motor that will be shared
        self.POS1_share = POS1_share
        ## Delta of the motor represents the speed of motor
        self.delta1_share = delta1_share
        ## Position of the motor that will be shared
        self.POS2_share = POS2_share
        ## Delta of the motor represents the speed of motor
        self.delta2_share = delta2_share
        ## System is to define what system is being called 1 or 2
        self.system = system
        
        
        # Define internal objects
        #
        ## The number of runs of the state machine
        self.runs = 0
        ## the number of times enabled is pushed
        self.enable_runs = 0
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Index number used to index each point of data collected
        self.index = 0
        ## The position data collected
        self.position_data = []
        ## The time data collected
        self.time_data = []
        ## The detla data collected
        self.delta_data = []
        ## State of this finite state machine
        self.state = 0
        self.start_time = 0
        self.encoder = 0
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
      
     def run(self):
            '''
                @brief Runs one iteration of the FSM
            '''
            
             
            ## Start time of gathering data
            self.present_time = utime.ticks_ms()/1000
            if self.state == S0_INIT:
                print('Welcome to motor controller')
                print('\'e\' to enable motors ')
                print('\'z\' to zero encoder 1')
                print('\'Z\' to ze ro encoder 2')
                print('\'p\' to print the encoder 1 position.')
                print('\'P\' to print the encoder 2 position.')
                print('\'d\' to print the detla of the encoder 1')
                print('\'D\' to print the detla of the encoder 2')
                print('\'m\' to set the speed of motor 1')
                print('\'M\' to set the speed of motor 1')
                print('\'g\'Collect enocder 1 data for 30 seconds')
                print('\'G\'Collect enocder 2 data for 30 seconds')
                print('\'s\' end data collection Permaturely')
                print('\'h\' print command instructions')
                
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
               if self.ser.any():
                   ## The character input by the user 
                   char_in = self.ser.read(1).decode()
                    
                   if(char_in == 'z'):
                        # Setting system state to 1 to zero encoder 1
                        self.sys_state.write(1)
                        print('Zeroing Position')
                        
                        
                   if(char_in == 'Z'):
                        # Setting system state to 1 to zero encoder 2
                        self.sys_state.write(2)
                        print('Zeroing Position')
                        
                   if(char_in == 'E' or char_in =='e'):
                       self.enable_runs += 1
                       
                       if self.enable_runs % 2 == 0:
                           self.sys_state.write(5)
                       else:
                           self.sys_state.write(4)
                       
                   elif(char_in == 'p'):
                       # printing position of encoder 1
                        print(self.POS1_share.read())
                        self.system.write(1)
                        
                   elif(char_in == 'P'):
                       # printing position of encoder 2
                        print(self.POS2_share.read())
                        self.system.write(2)
                        
                   elif(char_in == 'm'):
                       # set the speed of of motor 2
                       try:
                           self.duty = int(input('enter duty cycle -100 to 100\n'))
                           while self.duty > 100 or self.duty <-100:
                               self.duty = int(input('enter duty cycle -100 to 100\n'))
                       except:
                           print('Please Enter an Interger Value')
                       self.duty_share.write(self.duty)
                       self.sys_state.write(6)
                       self.system.write(1)
                       
                   elif(char_in == 'M'):
                       # set the speed of of motor 2
                       try:
                           self.duty = int(input('enter duty cycle -100 to 100\n'))
                           while self.duty > 100 or self.duty <-100:
                               self.duty = int(input('enter duty cycle -100 to 100\n'))
                       except:
                           print('Please Enter an Interger Value')
                       self.duty_share.write(self.duty)
                       self.sys_state.write(6)
                       self.system.write(2)
                    
                   elif(char_in == 'd'):
                       # printing delta of encoder 1
                        print(self.delta1_share.read())
                        self.system.write(1)
                        
                   elif(char_in == 'D'):
                       # printing delta of encoder 2
                        print(self.delta2_share.read())
                        self.system.write(2)
                       
                   elif(char_in == 'g'):
                        self.state = S2_GATHER_DATA
                        print('Gatheirng Position Data of encoder 1 . . .')
                        self.encoder = 1
                        
                   elif(char_in == 'G'):
                        self.state = S2_GATHER_DATA
                        print('Gatheirng Position Data of encoder 2 . . .')
                        self.encoder = 2
                        
                   elif(char_in == 'h' or char_in == 'h'):
                        self.state = S0_INIT
                              
            if self.state == S2_GATHER_DATA:
                ## start time is the time we start to take data
                ## current time the iteration starts on
                
                utime.sleep_ms(250)
                
                if self.encoder == 1:
                    self.position_data.append(str(self.POS1_share.read()))
                    self.delta_data.append(str(self.delta1_share.read()))
                    
                elif self.encoder == 2:
                    self.position_data.append(str(self.POS2_share.read()))
                    self.delta_data.append(str(self.delta2_share.read()))
                    
                self.time_data.append(str(self.start_time))
                
                if self.start_time >= 30:
                        self.state = S3_PRINT_DATA
                        self.start_time = 0
                        print('Data collection is complete.')
                        
                elif self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if(char_in == 's' or char_in == 'S'):
                            self.state = S3_PRINT_DATA
                            print('Data gathering has been halted.')
                            self.start_time = 0
                self.start_time += 0.25
                            
            
           
            if self.state == S3_PRINT_DATA: 
                utime.sleep_ms(250)
                #print('index', self.index)
                if self.index > len(self.position_data):
                    self.state = S1_WAIT_FOR_CHAR
                    self.position_data.clear()
                    self.time_data.clear()
                    self.index = 0
                elif self.index < len(self.position_data):
                     print(self.time_data[self.index], '[s] ,', self.position_data[self.index] , '[rad]',
                           self.delta_data[self.index], '[rad/s]')
                self.index += 1
            self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
      
                                        
                    
                    