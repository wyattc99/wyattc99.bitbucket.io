# -*- coding: utf-8 -*-
"""
@file           closedloop
@brief          This file create a closed loop driver.
@details        This file will create a closed loop driver class that will
                allow a user to interface with a task file. This driver file 
                will allow for the setting of the gain and computation of 
                values. 
@author         James Lavelle
@author         Wyatt Conner
@date           18 November 2021
"""

class Loop():
    """
    @brief      This class will create a closed loop controller
    @details    The closed loop controller class will create and construct a 
                closed loop controller that will allow the gain and pmw level 
                of the motor to be set so that the user task file can operate
                the system.
    """
    
    def __init__(self):
        """
        @brief          Constructs the parameters for the closed loop class
        @details        This function will construct and initialize the 
                        parameters for the closed loop class. These parameters
                        include the gain and the pmw level.
        @param   kp     The gain of the system.
        @param   duty    The duty of the system. 
        """
        ##kp is the gain of the system
        self.kp = 0
        ##duty is the duty of the system
        self.duty = 0
    
    def update(self, ref,  meas):
        """
        @brief          This function will compute the actuation value
        @details        This function will allow the computation and return of 
                        updated actuation value. This value will be based on 
                        the measured and reference values. 
        @param ref      The ref measured of the system. 
        @param meas     The omega measured of the system.
        @return         The duty of the function.
        """
  
        self.error = ref - meas
        self.duty = self.kp*self.error
        return self.duty
    
    def set_kp(self, kp):
        """
        @brief              This function will set the kp
        @details            This function will set the gain of the function by
                            setting the kp and the level to a set value. 
        @param      kp      This param is the gain of the system.
        """    
        
        self.kp = kp
        
    def get_kp(self):
        """
        @brief      This function will return the gain of the system
        @details    This function will find and return the gain of the system
                    or kp as it is called.
        @return     The gain or kp of the system will be returned.
        """
        
        return self.kp