# -*- coding: utf-8 -*-
"""
@file           taskloop
@brief          This file will create a user task for the closed loop driver
@details        This file will build off of labs 2 and 3 in order to create an
                updated user task file that will enable the user to operate 
                the files as a finite state 
@author         James Lavelle
@author         Wyatt Conner
@date           18 November 2021
"""

import utime
import pyb
import array
import math
from closedloop import Loop

class loop_task:
    """ 
        @brief      Motor task that calls the closed loop class.
        @details    Implements a finite state machine that determines the 
                     and set the speed of the motor. 
    """
    
    def __init__(self, period, flag, POS1_share, delta1_share, POS2_share, delta2_share, 
                  encoder1, encoder2, task1, task2, Motor1, Motor2, Motordrive, Loop):
        """
            @brief            Constructs a closed loop task.
            @details          The closed loop task is implemented as a finite 
                              state machine to move through the states and 
                              perform the various functions.
            @param flag       The flag keys used by the user. 
            @param period     The period of the system. 
            @param encoder1   The first encoder. 
            @param encoder2   The second encoder.
            @param task1      The first task.
            @param task2      The second task.
            @param Motor1     The first motor.
            @param Motor2     The second motor.
            @param Motordrive The driver of the motors.
        """
        
        ##flag is the flag keys used by the user
        self.flag = flag
        ##period is the period of the system
        self.period = period
        ##encoder1 is the first encoder
        self.encoder1 = encoder1
        ## POS1_share is the position of the first encoder
        self.POS1_share = POS1_share
        ## delta1_share is the speed of the first encoder
        self.delta1_share = delta1_share
        ## POS2_share is the position of the second encoder
        self.POS2_share = POS2_share
        ## delta2_share is the speed of the second encoder
        self.delta2_share = delta2_share
        ##encoder2 is the second encoder
        self.encoder2 = encoder2
        ##task1 is the first task
        self.task1 = task1
        ##task2 is the second task
        self.task2 = task2
        ##Motor1 is the first motor
        self.Motor1 = Motor1
        ##Motor2 is the second motor
        self.Motor2 = Motor2
        ##Motordrive is the driver of the motor
        self.Motordrive = Motordrive
        ##Loop is the loop class from the closed loop file
        self.Loop = Loop
        ##count is the count of the timer
        self.count = 0
        ##current_time is the current time
        self.current_time = utime.ticks_us()
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ##state is the state of the system
        self.state = 0
        ##index is the index of the system
        self.index = 0
        
    def run(self):
        """
        @brief          This function will run th
        @details        This function will run through various states of the 
                        finite state machine after recieving inputs from the 
                        user. 
        """
        
        ##the starting time of data collection
        current_time = utime.ticks_us()        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.state == 0:
                print('Welcome, press \'r\' to perform a step response')
                print('\'f\' Clear a fault condition')
                print('\'g\'Collect encoder data for 10 seconds')
                print('\'s\' End data collection Prematurely')
                print('\'h\' Print command instructions')
            
                self.state = 1
            
            if self.state == 1:
                print('In State 1. Waiting for input...')
                if self.ser.any():
                    ## The character input by the user 
                    char_in = self.ser.read(1).decode()                   
                    if(char_in == 'r' or char_in == 'R'):
                        print('Performing Step Response')
                        self.state == 2
                     
                    elif(char_in == 'f' or char_in == 'F'):
                        print('Fault condition clearing...')
                        self.state == 3
                                            
                    elif(char_in == 'g' or char_in == 'G'):
                        print('Gatheirng Encoder Data ...')
                        self.state == 4
                        
                    elif(char_in == 'h' or char_in == 'H'):
                        self.state == 0

            if self.state == 3:
                self.Motordrive.enable()
                self.state = 1
                    
            if self.state == 4:
                current_time = utime.ticks_us()
                
                utime.sleep_ms(250)
                
                if self.encoder == 1:
                    self.position_data.append(str(self.POS1_share.read()))
                    self.delta_data.append(str(self.delta1_share.read()))
                    
                elif self.encoder == 2:
                    self.position_data.append(str(self.POS2_share.read()))
                    self.delta_data.append(str(self.delta2_share.read()))
                    
                self.time_data.append(str(self.start_time))
                
                if self.start_time >= 30:
                        self.state = 5
                        self.start_time = 0
                        print('Data collection is complete.')
                        
                elif self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if(char_in == 's' or char_in == 'S'):
                            self.state = 5
                            print('Data gathering has been halted.')
                            self.start_time = 0
                self.start_time += 0.25
                
            if self.state == 5: 
                utime.sleep_ms(250)
                #print('index', self.index)
                if self.index > len(self.position_data):
                    self.state = 1
                    self.position_data.clear()
                    self.time_data.clear()
                    self.index = 0
                elif self.index < len(self.position_data):
                     print(self.time_data[self.index], '[s] ,', self.position_data[self.index] , '[rad]',
                           self.delta_data[self.index], '[rad/s]')
                self.index += 1
              
                    
    