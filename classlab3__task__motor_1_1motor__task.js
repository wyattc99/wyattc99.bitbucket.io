var classlab3__task__motor_1_1motor__task =
[
    [ "__init__", "classlab3__task__motor_1_1motor__task.html#a277c50636084de787ff9b91455879a60", null ],
    [ "run", "classlab3__task__motor_1_1motor__task.html#a182d4ee6e493f7a30951500ca6be7ec8", null ],
    [ "duty", "classlab3__task__motor_1_1motor__task.html#a3ff935be1fd264c677d75bf69c946ac1", null ],
    [ "duty_share", "classlab3__task__motor_1_1motor__task.html#a59e35f6b3a0c426a153b6781543ca6cb", null ],
    [ "Motor1", "classlab3__task__motor_1_1motor__task.html#add569bc9a89a0998828b9be4a905af04", null ],
    [ "Motor2", "classlab3__task__motor_1_1motor__task.html#a27b1fec689a9ffd6a153d6b1117a7611", null ],
    [ "Motordrive", "classlab3__task__motor_1_1motor__task.html#a28eeb4e2344ec371e18a513f580fbb77", null ],
    [ "next_time", "classlab3__task__motor_1_1motor__task.html#aa3645baa3e1b4259a6fbf97defa01019", null ],
    [ "period", "classlab3__task__motor_1_1motor__task.html#adde3c74bef54eae31cf74f681ea1812f", null ],
    [ "previous_duty", "classlab3__task__motor_1_1motor__task.html#aa3a4c9eab2baa124980a5a1002597078", null ],
    [ "runs", "classlab3__task__motor_1_1motor__task.html#aa212a1f8ada79a49d303631e567696de", null ],
    [ "sys_state", "classlab3__task__motor_1_1motor__task.html#a9b509c1fc3550f8cd233fa99efa14008", null ],
    [ "system", "classlab3__task__motor_1_1motor__task.html#ae1a74e40fefdad55202600480c03c9c6", null ]
];