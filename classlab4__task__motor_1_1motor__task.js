var classlab4__task__motor_1_1motor__task =
[
    [ "__init__", "classlab4__task__motor_1_1motor__task.html#a327cc946e7597a1ebd48f3b6432d63dc", null ],
    [ "run", "classlab4__task__motor_1_1motor__task.html#a03d7ccd1bf46de8d7ac8c5dc640378ed", null ],
    [ "duty", "classlab4__task__motor_1_1motor__task.html#a334f38626f5bd8bb098cda431f8e6663", null ],
    [ "duty_share", "classlab4__task__motor_1_1motor__task.html#a9019c836f5f524a0aa3de3fd02fd48f4", null ],
    [ "Motor1", "classlab4__task__motor_1_1motor__task.html#a7d9555ff54f499e65c77c1e3af3733ce", null ],
    [ "Motor2", "classlab4__task__motor_1_1motor__task.html#a5b3c34a40652827acc0e20454b3380b3", null ],
    [ "Motordrive", "classlab4__task__motor_1_1motor__task.html#affe4543885448f171a2c4a61f7ac73e6", null ],
    [ "next_time", "classlab4__task__motor_1_1motor__task.html#a9ac23ee0e8babebacdcd4a79c7ac655e", null ],
    [ "period", "classlab4__task__motor_1_1motor__task.html#aa54fff8439003241531f3319084478c9", null ],
    [ "previous_duty", "classlab4__task__motor_1_1motor__task.html#af30944d10fcd1ffb0eb248e396381dd4", null ],
    [ "runs", "classlab4__task__motor_1_1motor__task.html#a0b238c74de3396fdd1fe4c8ae56dab13", null ],
    [ "sys_state", "classlab4__task__motor_1_1motor__task.html#a13b4e5dcf8f529cbeeb5a41e70c86904", null ],
    [ "system", "classlab4__task__motor_1_1motor__task.html#afd0e5ca2242945275dfa71051ccacae9", null ]
];