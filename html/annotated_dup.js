var annotated_dup =
[
    [ "I2Cdriver", null, [
      [ "driver", "classI2Cdriver_1_1driver.html", "classI2Cdriver_1_1driver" ]
    ] ],
    [ "lab2_encoder", null, [
      [ "driver_encoder", "classlab2__encoder_1_1driver__encoder.html", "classlab2__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab2_shares", null, [
      [ "Queue", "classlab2__shares_1_1Queue.html", "classlab2__shares_1_1Queue" ],
      [ "Share", "classlab2__shares_1_1Share.html", "classlab2__shares_1_1Share" ]
    ] ],
    [ "lab2_task_encoder", null, [
      [ "encoder_task", "classlab2__task__encoder_1_1encoder__task.html", "classlab2__task__encoder_1_1encoder__task" ]
    ] ],
    [ "lab2_user_task", null, [
      [ "task_user", "classlab2__user__task_1_1task__user.html", "classlab2__user__task_1_1task__user" ]
    ] ],
    [ "lab3_encoder", null, [
      [ "driver_encoder", "classlab3__encoder_1_1driver__encoder.html", "classlab3__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab3_motordriver", null, [
      [ "Driver", "classlab3__motordriver_1_1Driver.html", "classlab3__motordriver_1_1Driver" ],
      [ "Motor", "classlab3__motordriver_1_1Motor.html", "classlab3__motordriver_1_1Motor" ]
    ] ],
    [ "lab3_shares", null, [
      [ "Queue", "classlab3__shares_1_1Queue.html", "classlab3__shares_1_1Queue" ],
      [ "Share", "classlab3__shares_1_1Share.html", "classlab3__shares_1_1Share" ]
    ] ],
    [ "lab3_task_motor", null, [
      [ "motor_task", "classlab3__task__motor_1_1motor__task.html", "classlab3__task__motor_1_1motor__task" ]
    ] ],
    [ "lab3_user_task", null, [
      [ "task_user", "classlab3__user__task_1_1task__user.html", "classlab3__user__task_1_1task__user" ]
    ] ],
    [ "lab4_closedloop", null, [
      [ "Loop", "classlab4__closedloop_1_1Loop.html", "classlab4__closedloop_1_1Loop" ]
    ] ],
    [ "lab4_task_motor", null, [
      [ "motor_task", "classlab4__task__motor_1_1motor__task.html", "classlab4__task__motor_1_1motor__task" ]
    ] ],
    [ "lab4_taskloop", null, [
      [ "loop_task", "classlab4__taskloop_1_1loop__task.html", "classlab4__taskloop_1_1loop__task" ]
    ] ]
];