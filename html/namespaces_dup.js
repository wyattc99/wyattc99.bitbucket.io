var namespaces_dup =
[
    [ "Conner_305_0x01", null, [
      [ "onButtonPressFCN", "Conner__305__0x01_8py.html#a11894049b79d699e7799c48cb730848d", null ],
      [ "update_SIN", "Conner__305__0x01_8py.html#aed65fe4899f993673eb4a9b1e68e14e2", null ],
      [ "update_SQW", "Conner__305__0x01_8py.html#ae97af7d15a675599f3ef76e1f178d3b1", null ],
      [ "update_STW", "Conner__305__0x01_8py.html#aef188e1803848eeeed63e2d35c541f30", null ],
      [ "button", "Conner__305__0x01_8py.html#a2fcd9f347f1c0da400354115e8689017", null ],
      [ "ButtonInt", "Conner__305__0x01_8py.html#adb2577310dd91d46be39f8699a741271", null ],
      [ "Current_Time", "Conner__305__0x01_8py.html#aaac1740b16dd5edef0d84246c173e943", null ],
      [ "pinA5", "Conner__305__0x01_8py.html#a3bb21699b4a93b266aca9068a17f7b30", null ],
      [ "pinC13", "Conner__305__0x01_8py.html#a7084c44f56eb73635f48d507c23c8d1b", null ],
      [ "run", "Conner__305__0x01_8py.html#aaa92d96c59fdf1ddbca546ae664f930c", null ],
      [ "startTime", "Conner__305__0x01_8py.html#a560e7a0532ae6998abea2dfd35ee53e8", null ],
      [ "state", "Conner__305__0x01_8py.html#af788bc55f8e15359db24fa0ffdb4d992", null ],
      [ "stopTime", "Conner__305__0x01_8py.html#a18a8221cb45e974a7bcfbe2266c067c0", null ],
      [ "t2ch1", "Conner__305__0x01_8py.html#a1baea9e4c014feda23d20be0cd6abbc5", null ],
      [ "tim2", "Conner__305__0x01_8py.html#a9b14efdfa65fa4c4f3ced7b7b8c24c68", null ]
    ] ],
    [ "I2Cdriver", null, [
      [ "driver", "classI2Cdriver_1_1driver.html", "classI2Cdriver_1_1driver" ]
    ] ],
    [ "lab", "namespacelab.html", null ],
    [ "lab2_encoder", null, [
      [ "driver_encoder", "classlab2__encoder_1_1driver__encoder.html", "classlab2__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab2_shares", null, [
      [ "Queue", "classlab2__shares_1_1Queue.html", "classlab2__shares_1_1Queue" ],
      [ "Share", "classlab2__shares_1_1Share.html", "classlab2__shares_1_1Share" ]
    ] ],
    [ "lab2_task_encoder", null, [
      [ "encoder_task", "classlab2__task__encoder_1_1encoder__task.html", "classlab2__task__encoder_1_1encoder__task" ]
    ] ],
    [ "lab2_user_task", null, [
      [ "task_user", "classlab2__user__task_1_1task__user.html", "classlab2__user__task_1_1task__user" ]
    ] ],
    [ "lab3_encoder", null, [
      [ "driver_encoder", "classlab3__encoder_1_1driver__encoder.html", "classlab3__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab3_motordriver", null, [
      [ "Driver", "classlab3__motordriver_1_1Driver.html", "classlab3__motordriver_1_1Driver" ],
      [ "Motor", "classlab3__motordriver_1_1Motor.html", "classlab3__motordriver_1_1Motor" ]
    ] ],
    [ "lab3_shares", null, [
      [ "Queue", "classlab3__shares_1_1Queue.html", "classlab3__shares_1_1Queue" ],
      [ "Share", "classlab3__shares_1_1Share.html", "classlab3__shares_1_1Share" ]
    ] ],
    [ "lab3_task_motor", null, [
      [ "motor_task", "classlab3__task__motor_1_1motor__task.html", "classlab3__task__motor_1_1motor__task" ]
    ] ],
    [ "lab3_user_task", null, [
      [ "task_user", "classlab3__user__task_1_1task__user.html", "classlab3__user__task_1_1task__user" ]
    ] ],
    [ "lab4_closedloop", null, [
      [ "Loop", "classlab4__closedloop_1_1Loop.html", "classlab4__closedloop_1_1Loop" ]
    ] ],
    [ "lab4_task_motor", null, [
      [ "motor_task", "classlab4__task__motor_1_1motor__task.html", "classlab4__task__motor_1_1motor__task" ]
    ] ],
    [ "lab4_taskloop", null, [
      [ "loop_task", "classlab4__taskloop_1_1loop__task.html", "classlab4__taskloop_1_1loop__task" ]
    ] ],
    [ "Lab_0x01", "namespaceLab__0x01.html", null ],
    [ "Lab_0x02", "namespaceLab__0x02.html", null ],
    [ "Lab_0x03", "namespaceLab__0x03.html", null ],
    [ "Lab_0x04", "namespaceLab__0x04.html", null ]
];