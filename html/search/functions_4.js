var searchData=
[
  ['get_0',['get',['../classlab2__shares_1_1Queue.html#a986a827fd14646b32d144c8377db46f6',1,'lab2_shares.Queue.get()'],['../classlab3__shares_1_1Queue.html#a1ed5008062980829146d59cdad0ced52',1,'lab3_shares.Queue.get()']]],
  ['get_5fcal_5fcoef_1',['get_cal_coef',['../classI2Cdriver_1_1driver.html#a3e32be08f3b6017b9d3c930a976b3aca',1,'I2Cdriver::driver']]],
  ['get_5fcal_5fstatus_2',['get_cal_status',['../classI2Cdriver_1_1driver.html#a9e03621482357ae5163226f712fb63ec',1,'I2Cdriver::driver']]],
  ['get_5fdelta_3',['get_delta',['../classlab2__encoder_1_1driver__encoder.html#afb5d5c546bf65408767a6eeb4b996701',1,'lab2_encoder.driver_encoder.get_delta()'],['../classlab3__encoder_1_1driver__encoder.html#a466f968d23c8b5d21d1d7bffdd1158a4',1,'lab3_encoder.driver_encoder.get_delta()']]],
  ['get_5fkp_4',['get_kp',['../classlab4__closedloop_1_1Loop.html#ac20b92ce76ccd6732c7c6744933e7456',1,'lab4_closedloop::Loop']]],
  ['get_5fposition_5',['get_position',['../classlab2__encoder_1_1driver__encoder.html#a7a98c0cea2a7666b4f8bf93cd3e571da',1,'lab2_encoder.driver_encoder.get_position()'],['../classlab3__encoder_1_1driver__encoder.html#a3c36beee08e12735da2bd50f09f56970',1,'lab3_encoder.driver_encoder.get_position()']]]
];
