var annotated_dup =
[
    [ "Driver_I2C", null, [
      [ "I2C_Driver", "classDriver__I2C_1_1I2C__Driver.html", "classDriver__I2C_1_1I2C__Driver" ]
    ] ],
    [ "Driver_Motor", null, [
      [ "Driver", "classDriver__Motor_1_1Driver.html", "classDriver__Motor_1_1Driver" ],
      [ "Motor", "classDriver__Motor_1_1Motor.html", "classDriver__Motor_1_1Motor" ]
    ] ],
    [ "Driver_Touch", null, [
      [ "Touch_Driver", "classDriver__Touch_1_1Touch__Driver.html", "classDriver__Touch_1_1Touch__Driver" ]
    ] ],
    [ "I2Cdriver", null, [
      [ "driver", "classI2Cdriver_1_1driver.html", "classI2Cdriver_1_1driver" ]
    ] ],
    [ "lab2_encoder", null, [
      [ "driver_encoder", "classlab2__encoder_1_1driver__encoder.html", "classlab2__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab2_shares", null, [
      [ "Queue", "classlab2__shares_1_1Queue.html", "classlab2__shares_1_1Queue" ],
      [ "Share", "classlab2__shares_1_1Share.html", "classlab2__shares_1_1Share" ]
    ] ],
    [ "lab2_task_encoder", null, [
      [ "encoder_task", "classlab2__task__encoder_1_1encoder__task.html", "classlab2__task__encoder_1_1encoder__task" ]
    ] ],
    [ "lab2_user_task", null, [
      [ "task_user", "classlab2__user__task_1_1task__user.html", "classlab2__user__task_1_1task__user" ]
    ] ],
    [ "lab3_encoder", null, [
      [ "driver_encoder", "classlab3__encoder_1_1driver__encoder.html", "classlab3__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab3_motordriver", null, [
      [ "Driver", "classlab3__motordriver_1_1Driver.html", "classlab3__motordriver_1_1Driver" ],
      [ "Motor", "classlab3__motordriver_1_1Motor.html", "classlab3__motordriver_1_1Motor" ]
    ] ],
    [ "lab3_shares", null, [
      [ "Queue", "classlab3__shares_1_1Queue.html", "classlab3__shares_1_1Queue" ],
      [ "Share", "classlab3__shares_1_1Share.html", "classlab3__shares_1_1Share" ]
    ] ],
    [ "lab3_task_motor", null, [
      [ "motor_task", "classlab3__task__motor_1_1motor__task.html", "classlab3__task__motor_1_1motor__task" ]
    ] ],
    [ "lab3_user_task", null, [
      [ "task_user", "classlab3__user__task_1_1task__user.html", "classlab3__user__task_1_1task__user" ]
    ] ],
    [ "lab4_closedloop", null, [
      [ "Loop", "classlab4__closedloop_1_1Loop.html", "classlab4__closedloop_1_1Loop" ]
    ] ],
    [ "lab4_task_motor", null, [
      [ "motor_task", "classlab4__task__motor_1_1motor__task.html", "classlab4__task__motor_1_1motor__task" ]
    ] ],
    [ "lab4_taskloop", null, [
      [ "loop_task", "classlab4__taskloop_1_1loop__task.html", "classlab4__taskloop_1_1loop__task" ]
    ] ],
    [ "Lab5_IMU_driver", null, [
      [ "driver", "classLab5__IMU__driver_1_1driver.html", "classLab5__IMU__driver_1_1driver" ]
    ] ],
    [ "read_and_write", null, [
      [ "Read_Write_Text", "classread__and__write_1_1Read__Write__Text.html", "classread__and__write_1_1Read__Write__Text" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "Task_Calibration", null, [
      [ "calibration", "classTask__Calibration_1_1calibration.html", "classTask__Calibration_1_1calibration" ]
    ] ],
    [ "Task_Data_Collection", null, [
      [ "Data_Collection", "classTask__Data__Collection_1_1Data__Collection.html", "classTask__Data__Collection_1_1Data__Collection" ]
    ] ],
    [ "Task_Speed_Controller", null, [
      [ "Task_Controller", "classTask__Speed__Controller_1_1Task__Controller.html", "classTask__Speed__Controller_1_1Task__Controller" ]
    ] ],
    [ "Task_User", null, [
      [ "user_task", "classTask__User_1_1user__task.html", "classTask__User_1_1user__task" ]
    ] ]
];