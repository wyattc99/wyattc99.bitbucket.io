var classTask__Data__Collection_1_1Data__Collection =
[
    [ "run", "classTask__Data__Collection_1_1Data__Collection.html#a50c6e1264409069a706101fe88d9d087", null ],
    [ "cal_task", "classTask__Data__Collection_1_1Data__Collection.html#afa4bed36e5f90eae78152a9e606079fe", null ],
    [ "Euler", "classTask__Data__Collection_1_1Data__Collection.html#a51cb227ff9b3295dd3f6ce98cc04695b", null ],
    [ "Flag", "classTask__Data__Collection_1_1Data__Collection.html#a363dc6590aefe638296974c438014732", null ],
    [ "IMU", "classTask__Data__Collection_1_1Data__Collection.html#a3d974a171fdaecbeb855763ddb087543", null ],
    [ "mode", "classTask__Data__Collection_1_1Data__Collection.html#a6056f04e5969f0af73a8792eabc95236", null ],
    [ "pitch", "classTask__Data__Collection_1_1Data__Collection.html#a366ac3597d1d49bd6a97d41eefb2703c", null ],
    [ "pitch_vel", "classTask__Data__Collection_1_1Data__Collection.html#af62c81cc399f97b7010e10df77df2e01", null ],
    [ "roll", "classTask__Data__Collection_1_1Data__Collection.html#a65888dcb195a33ad1549e69afbcf4f81", null ],
    [ "roll_vel", "classTask__Data__Collection_1_1Data__Collection.html#a755509e16824667b49fe59b38c187491", null ],
    [ "RT_coef", "classTask__Data__Collection_1_1Data__Collection.html#ac1cb95f37d78abf416a6f18634ac6103", null ],
    [ "touch", "classTask__Data__Collection_1_1Data__Collection.html#abb981e65bba0b2900c99be33566cfd6d", null ],
    [ "x_correct", "classTask__Data__Collection_1_1Data__Collection.html#af4cefd3b1c2ed6440fd3fced3a090a46", null ],
    [ "xcord", "classTask__Data__Collection_1_1Data__Collection.html#a619833a88ac43b4c4655160a45e33fcc", null ],
    [ "xscan", "classTask__Data__Collection_1_1Data__Collection.html#a0fe39352cff4d6b473b01dca6f0a5b76", null ],
    [ "y_correct", "classTask__Data__Collection_1_1Data__Collection.html#a268e28e15b45c9e7d3ec63ec00fb47d7", null ],
    [ "ycord", "classTask__Data__Collection_1_1Data__Collection.html#a2ae2c76f82bf56560b3ffecfd5ba7612", null ],
    [ "yscan", "classTask__Data__Collection_1_1Data__Collection.html#ae1a25308aed1a8d8b2ec8e91a112f797", null ],
    [ "zcord", "classTask__Data__Collection_1_1Data__Collection.html#a24dedb0e316b774776b7e3848941861a", null ],
    [ "zscan", "classTask__Data__Collection_1_1Data__Collection.html#a8a95c8aecbbed8dc6cf7720642c86fb8", null ]
];