var files_dup =
[
    [ "Conner_305_0x01.py", "Conner__305__0x01_8py.html", "Conner__305__0x01_8py" ],
    [ "Driver_I2C.py", "Driver__I2C_8py.html", [
      [ "Driver_I2C.I2C_Driver", "classDriver__I2C_1_1I2C__Driver.html", "classDriver__I2C_1_1I2C__Driver" ]
    ] ],
    [ "Driver_Motor.py", "Driver__Motor_8py.html", "Driver__Motor_8py" ],
    [ "Driver_Touch.py", "Driver__Touch_8py.html", [
      [ "Driver_Touch.Touch_Driver", "classDriver__Touch_1_1Touch__Driver.html", "classDriver__Touch_1_1Touch__Driver" ]
    ] ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "read_and_write.py", "read__and__write_8py.html", [
      [ "read_and_write.Read_Write_Text", "classread__and__write_1_1Read__Write__Text.html", "classread__and__write_1_1Read__Write__Text" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "Task_Calibration.py", "Task__Calibration_8py.html", [
      [ "Task_Calibration.calibration", "classTask__Calibration_1_1calibration.html", "classTask__Calibration_1_1calibration" ]
    ] ],
    [ "Task_Data_Collection.py", "Task__Data__Collection_8py.html", [
      [ "Task_Data_Collection.Data_Collection", "classTask__Data__Collection_1_1Data__Collection.html", "classTask__Data__Collection_1_1Data__Collection" ]
    ] ],
    [ "Task_Speed_Controller.py", "Task__Speed__Controller_8py.html", [
      [ "Task_Speed_Controller.Task_Controller", "classTask__Speed__Controller_1_1Task__Controller.html", "classTask__Speed__Controller_1_1Task__Controller" ]
    ] ],
    [ "Task_User.py", "Task__User_8py.html", "Task__User_8py" ]
];