var namespaces_dup =
[
    [ "Conner_305_0x01", null, [
      [ "onButtonPressFCN", "Conner__305__0x01_8py.html#a11894049b79d699e7799c48cb730848d", null ],
      [ "update_SIN", "Conner__305__0x01_8py.html#aed65fe4899f993673eb4a9b1e68e14e2", null ],
      [ "update_SQW", "Conner__305__0x01_8py.html#ae97af7d15a675599f3ef76e1f178d3b1", null ],
      [ "update_STW", "Conner__305__0x01_8py.html#aef188e1803848eeeed63e2d35c541f30", null ],
      [ "button", "Conner__305__0x01_8py.html#a2fcd9f347f1c0da400354115e8689017", null ],
      [ "ButtonInt", "Conner__305__0x01_8py.html#adb2577310dd91d46be39f8699a741271", null ],
      [ "Current_Time", "Conner__305__0x01_8py.html#aaac1740b16dd5edef0d84246c173e943", null ],
      [ "pinA5", "Conner__305__0x01_8py.html#a3bb21699b4a93b266aca9068a17f7b30", null ],
      [ "pinC13", "Conner__305__0x01_8py.html#a7084c44f56eb73635f48d507c23c8d1b", null ],
      [ "run", "Conner__305__0x01_8py.html#aaa92d96c59fdf1ddbca546ae664f930c", null ],
      [ "startTime", "Conner__305__0x01_8py.html#a560e7a0532ae6998abea2dfd35ee53e8", null ],
      [ "state", "Conner__305__0x01_8py.html#af788bc55f8e15359db24fa0ffdb4d992", null ],
      [ "stopTime", "Conner__305__0x01_8py.html#a18a8221cb45e974a7bcfbe2266c067c0", null ],
      [ "t2ch1", "Conner__305__0x01_8py.html#a1baea9e4c014feda23d20be0cd6abbc5", null ],
      [ "tim2", "Conner__305__0x01_8py.html#a9b14efdfa65fa4c4f3ced7b7b8c24c68", null ]
    ] ],
    [ "Driver_I2C", null, [
      [ "I2C_Driver", "classDriver__I2C_1_1I2C__Driver.html", "classDriver__I2C_1_1I2C__Driver" ]
    ] ],
    [ "Driver_Motor", null, [
      [ "Driver", "classDriver__Motor_1_1Driver.html", "classDriver__Motor_1_1Driver" ],
      [ "Motor", "classDriver__Motor_1_1Motor.html", "classDriver__Motor_1_1Motor" ],
      [ "motor_1", "Driver__Motor_8py.html#a85d06c0f92ed408e28cf9d197bab83b9", null ],
      [ "motor_2", "Driver__Motor_8py.html#a4e0ff2fb46a6d9d79b80257bb00dbf2c", null ],
      [ "motor_driver", "Driver__Motor_8py.html#a6b06c4de356c6090d6eb6cfd1dbc1e7a", null ]
    ] ],
    [ "Driver_Touch", null, [
      [ "Touch_Driver", "classDriver__Touch_1_1Touch__Driver.html", "classDriver__Touch_1_1Touch__Driver" ]
    ] ],
    [ "I2Cdriver", null, [
      [ "driver", "classI2Cdriver_1_1driver.html", "classI2Cdriver_1_1driver" ]
    ] ],
    [ "lab", "namespacelab.html", null ],
    [ "Lab0x05", "namespaceLab0x05.html", null ],
    [ "lab2_encoder", null, [
      [ "driver_encoder", "classlab2__encoder_1_1driver__encoder.html", "classlab2__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab2_shares", null, [
      [ "Queue", "classlab2__shares_1_1Queue.html", "classlab2__shares_1_1Queue" ],
      [ "Share", "classlab2__shares_1_1Share.html", "classlab2__shares_1_1Share" ]
    ] ],
    [ "lab2_task_encoder", null, [
      [ "encoder_task", "classlab2__task__encoder_1_1encoder__task.html", "classlab2__task__encoder_1_1encoder__task" ]
    ] ],
    [ "lab2_user_task", null, [
      [ "task_user", "classlab2__user__task_1_1task__user.html", "classlab2__user__task_1_1task__user" ]
    ] ],
    [ "lab3_encoder", null, [
      [ "driver_encoder", "classlab3__encoder_1_1driver__encoder.html", "classlab3__encoder_1_1driver__encoder" ]
    ] ],
    [ "lab3_motordriver", null, [
      [ "Driver", "classlab3__motordriver_1_1Driver.html", "classlab3__motordriver_1_1Driver" ],
      [ "Motor", "classlab3__motordriver_1_1Motor.html", "classlab3__motordriver_1_1Motor" ]
    ] ],
    [ "lab3_shares", null, [
      [ "Queue", "classlab3__shares_1_1Queue.html", "classlab3__shares_1_1Queue" ],
      [ "Share", "classlab3__shares_1_1Share.html", "classlab3__shares_1_1Share" ]
    ] ],
    [ "lab3_task_motor", null, [
      [ "motor_task", "classlab3__task__motor_1_1motor__task.html", "classlab3__task__motor_1_1motor__task" ]
    ] ],
    [ "lab3_user_task", null, [
      [ "task_user", "classlab3__user__task_1_1task__user.html", "classlab3__user__task_1_1task__user" ]
    ] ],
    [ "lab4_closedloop", null, [
      [ "Loop", "classlab4__closedloop_1_1Loop.html", "classlab4__closedloop_1_1Loop" ]
    ] ],
    [ "lab4_task_motor", null, [
      [ "motor_task", "classlab4__task__motor_1_1motor__task.html", "classlab4__task__motor_1_1motor__task" ]
    ] ],
    [ "lab4_taskloop", null, [
      [ "loop_task", "classlab4__taskloop_1_1loop__task.html", "classlab4__taskloop_1_1loop__task" ]
    ] ],
    [ "Lab5_IMU_driver", null, [
      [ "driver", "classLab5__IMU__driver_1_1driver.html", "classLab5__IMU__driver_1_1driver" ]
    ] ],
    [ "Lab_0x01", "namespaceLab__0x01.html", null ],
    [ "Lab_0x02", "namespaceLab__0x02.html", null ],
    [ "Lab_0x03", "namespaceLab__0x03.html", null ],
    [ "Lab_0x04", "namespaceLab__0x04.html", null ],
    [ "read_and_write", null, [
      [ "Read_Write_Text", "classread__and__write_1_1Read__Write__Text.html", "classread__and__write_1_1Read__Write__Text" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "Task_Calibration", null, [
      [ "calibration", "classTask__Calibration_1_1calibration.html", "classTask__Calibration_1_1calibration" ]
    ] ],
    [ "Task_Data_Collection", null, [
      [ "Data_Collection", "classTask__Data__Collection_1_1Data__Collection.html", "classTask__Data__Collection_1_1Data__Collection" ]
    ] ],
    [ "Task_Speed_Controller", null, [
      [ "Task_Controller", "classTask__Speed__Controller_1_1Task__Controller.html", "classTask__Speed__Controller_1_1Task__Controller" ]
    ] ],
    [ "Task_User", null, [
      [ "user_task", "classTask__User_1_1user__task.html", "classTask__User_1_1user__task" ],
      [ "S0_INIT", "Task__User_8py.html#a121f1ac454a64980fccfb1e605228f00", null ],
      [ "S1_WAIT_FOR_CHAR", "Task__User_8py.html#ac0fa40103dc3bf6d2699da5b4d6d0abf", null ],
      [ "S2_GATHER_DATA", "Task__User_8py.html#a53acedc26d89b67f6c8ed714451c58c5", null ],
      [ "S3_CLEAR_FAULT", "Task__User_8py.html#ad15d61ef690bbe434d5cabb66d7aeec5", null ],
      [ "S4_BALANCE_BALL", "Task__User_8py.html#ad62a7d30f84d498de52d6aa6decfd022", null ],
      [ "S5_PRINT_DATA", "Task__User_8py.html#a84e4005e0b318a7a40ca7ec2a4e482d8", null ]
    ] ],
    [ "Term_Project", "namespaceTerm__Project.html", null ]
];