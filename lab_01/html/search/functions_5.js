var searchData=
[
  ['get_0',['get',['../classlab2__shares_1_1Queue.html#a986a827fd14646b32d144c8377db46f6',1,'lab2_shares.Queue.get()'],['../classlab3__shares_1_1Queue.html#a1ed5008062980829146d59cdad0ced52',1,'lab3_shares.Queue.get()'],['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get()']]],
  ['get_5fcal_5fcoef_1',['get_cal_coef',['../classI2Cdriver_1_1driver.html#a3e32be08f3b6017b9d3c930a976b3aca',1,'I2Cdriver.driver.get_cal_coef()'],['../classLab5__IMU__driver_1_1driver.html#a7c3bbeb2b90987a0e57c974edab406cb',1,'Lab5_IMU_driver.driver.get_cal_coef()']]],
  ['get_5fcal_5fstatus_2',['get_cal_status',['../classDriver__I2C_1_1I2C__Driver.html#a7220af59119efba389691573b64b585e',1,'Driver_I2C.I2C_Driver.get_cal_status()'],['../classI2Cdriver_1_1driver.html#a9e03621482357ae5163226f712fb63ec',1,'I2Cdriver.driver.get_cal_status()'],['../classLab5__IMU__driver_1_1driver.html#a314cc2ff9f04ef057cd56c649fe4861b',1,'Lab5_IMU_driver.driver.get_cal_status()']]],
  ['get_5fdelta_3',['get_delta',['../classlab2__encoder_1_1driver__encoder.html#afb5d5c546bf65408767a6eeb4b996701',1,'lab2_encoder.driver_encoder.get_delta()'],['../classlab3__encoder_1_1driver__encoder.html#a466f968d23c8b5d21d1d7bffdd1158a4',1,'lab3_encoder.driver_encoder.get_delta()']]],
  ['get_5fk_5frt_4',['get_k_RT',['../classTask__Calibration_1_1calibration.html#a7065bdde82729c8af2772e1a79ce5f85',1,'Task_Calibration::calibration']]],
  ['get_5fkp_5',['get_kp',['../classlab4__closedloop_1_1Loop.html#ac20b92ce76ccd6732c7c6744933e7456',1,'lab4_closedloop::Loop']]],
  ['get_5fposition_6',['get_position',['../classlab2__encoder_1_1driver__encoder.html#a7a98c0cea2a7666b4f8bf93cd3e571da',1,'lab2_encoder.driver_encoder.get_position()'],['../classlab3__encoder_1_1driver__encoder.html#a3c36beee08e12735da2bd50f09f56970',1,'lab3_encoder.driver_encoder.get_position()']]]
];
