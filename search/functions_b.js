var searchData=
[
  ['scan_5fx_0',['scan_x',['../classDriver__Touch_1_1Touch__Driver.html#a05682aa355ffcc600aad8e4a53a8c337',1,'Driver_Touch::Touch_Driver']]],
  ['scan_5fy_1',['scan_y',['../classDriver__Touch_1_1Touch__Driver.html#aab100fd98a829c7eaf8aa73e6f860f43',1,'Driver_Touch::Touch_Driver']]],
  ['scan_5fz_2',['scan_z',['../classDriver__Touch_1_1Touch__Driver.html#aa63c4084fcd41ce7cdad81bd0e7ed296',1,'Driver_Touch::Touch_Driver']]],
  ['set_5fduty_3',['set_duty',['../classDriver__Motor_1_1Motor.html#add55be73c9e75e79f085490b1fce1b22',1,'Driver_Motor.Motor.set_duty()'],['../classlab3__motordriver_1_1Motor.html#a12aef6650963f13c6525308d69f4afe0',1,'lab3_motordriver.Motor.set_duty()']]],
  ['set_5fkp_4',['set_kp',['../classlab4__closedloop_1_1Loop.html#afad1b29e81f0f0ca86186c5561ac271f',1,'lab4_closedloop::Loop']]],
  ['set_5foperating_5fmode_5',['set_operating_mode',['../classDriver__I2C_1_1I2C__Driver.html#a77d41e3bccdb48a341eefc258514c8b2',1,'Driver_I2C.I2C_Driver.set_operating_mode()'],['../classI2Cdriver_1_1driver.html#a3b47b34d11164d7e4caebcfecf888e08',1,'I2Cdriver.driver.set_operating_mode()'],['../classLab5__IMU__driver_1_1driver.html#a3752cc70e81597d3d5c96c8769fb85c7',1,'Lab5_IMU_driver.driver.set_operating_mode()']]],
  ['set_5fposition_6',['set_position',['../classlab2__encoder_1_1driver__encoder.html#aeb414810dc4b16d85f368600ed67969b',1,'lab2_encoder.driver_encoder.set_position()'],['../classlab3__encoder_1_1driver__encoder.html#aaa22c5ac7cf4bd5f1f9484a82dd80882',1,'lab3_encoder.driver_encoder.set_position()']]]
];
