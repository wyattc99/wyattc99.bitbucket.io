var indexSectionsWithContent =
{
  0: "_abcdefgiklmnopqrstuwxyz",
  1: "cdeilmqrstu",
  2: "lt",
  3: "cdmrst",
  4: "_cdefgmnoprsuw",
  5: "abcdefiklmnprstxyz",
  6: "w"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

